/**
 * @author Tres Finocchiaro
 *
 * Copyright (C) 2013 Tres Finocchiaro, QZ Industries
 *
 * IMPORTANT:  This software is dual-licensed
 *
 * LGPL 2.1
 * This is free software.  This software and source code are released under 
 * the "LGPL 2.1 License".  A copy of this license should be distributed with 
 * this software. http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * QZ INDUSTRIES SOURCE CODE LICENSE
 * This software and source code *may* instead be distributed under the 
 * "QZ Industries Source Code License", available by request ONLY.  If source 
 * code for this project is to be made proprietary for an individual and/or a
 * commercial entity, written permission via a copy of the "QZ Industries Source
 * Code License" must be obtained first.  If you've obtained a copy of the 
 * proprietary license, the terms and conditions of the license apply only to 
 * the licensee identified in the agreement.  Only THEN may the LGPL 2.1 license
 * be voided.
 *
 */
package qz;
import jssc.SerialPort;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import java.awt.print.PrinterException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
/**
 * Cannot be run interactively.  Display error and exit.
 *
 * @author tfino
 */
public class Main {
    public static final String VERSION = "1.8.7";
    private static final String CLEAR_SCREEN = "1B40";
    private static final long serialVersionUID = 2787955484074291340L;
    public static final int APPEND_XML = 1;
    public static final int APPEND_RAW = 2;
    public static final int APPEND_IMAGE = 3;
    public static final int APPEND_IMAGE_PS = 4;
    public static final int APPEND_PDF = 8;
    public static final int APPEND_HTML = 16;
    //    private JSObject window = null;
    private LanguageType lang;
    private int appendType;
    private long sleep;
    private PrintService ps;
    private PrintRaw printRaw;
    private SerialIO serialIO;
    private PrintPostScript printPS;
    private PrintHTML printHTML;
    //private NetworkHashMap networkHashMap;
    private NetworkUtilities networkUtilities;
    private Throwable t;
    private PaperFormat paperSize;
    private boolean startFindingPrinters;
    private boolean doneFindingPrinters;
    private boolean startPrinting;
    private boolean donePrinting;
    private boolean startFindingNetwork;
    private boolean doneFindingNetwork;
    private boolean startAppending;
    private boolean doneAppending;
    private boolean startFindingPorts;
    private boolean doneFindingPorts;
    private boolean startSending;
    private boolean doneSending;
    private boolean autoSetSerialProperties = false;
    private boolean startOpeningPort;
    private boolean doneOpeningPort;
    private boolean startClosingPort;
    private boolean doneClosingPort;
    private String serialPortName;
    private int serialPortIndex = -1;
    private boolean running;
    private boolean reprint;
    private boolean psPrint;
    private boolean htmlPrint;
    private boolean alternatePrint;
    private boolean logFeaturesPS;
    private int imageX = 0;
    private int imageY = 0;
    private int dotDensity = 32;
    private boolean allowMultiple;
    //private double[] psMargin;
    private String jobName = "";
    private String file;
    private String xmlTag;
    private String printer;
    //private String orientation;
    //private Boolean maintainAspect;
    private int copies = -1;
    private Charset charset = Charset.defaultCharset();
    //private String pageBreak; // For spooling pages one at a time
    private int documentsPerSpool = 0;
    private String endOfDocument;
    //    public Main(String args[]) {
//
//    }
    public static byte[] hex2Byte(String str) {
        byte[] bytes = new byte[str.length() / 2];
        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = (byte) Integer
                    .parseInt(str.substring(2 * i, 2 * i + 2), 16);
        }
        return bytes;
    }
    public static byte[] cls() {
        return hex2Byte(CLEAR_SCREEN);
    }
    public void appendHTML(String html) {
        getPrintHTML().append(html);
    }
    public boolean isAlternatePrinting() {
        return this.alternatePrint;
    }
    public Charset getCharset() {
        return this.charset;
    }
    private PrintRaw getPrintRaw() {
        if (this.printRaw == null) {
            this.printRaw = new PrintRaw();
            this.printRaw.setPrintParameters(this);
        }
        return printRaw;
    }
    private boolean isRawAutoSpooling() throws UnsupportedEncodingException {
        return documentsPerSpool > 0 && endOfDocument != null && !getPrintRaw().isClear() && getPrintRaw().contains(endOfDocument);
    }
    public boolean getLogPostScriptFeatures() {
        return this.logFeaturesPS;
    }
    public String getJobName() {
        return jobName;
    }
    public int getCopies() {
        if (copies > 0) {
            return copies;
        } else {
            return 1;
        }
    }
    public PrintService getPrintService() {
        return ps;
    }
    public PaperFormat getPaperSize() {
        return paperSize;
    }
    private PrintPostScript getPrintPS() {
        if (this.printPS == null) {
            this.printPS = new PrintPostScript();
            this.printPS.setPrintParameters(this);
        }
        return printPS;
    }
    private PrintHTML getPrintHTML() {
        if (this.printHTML == null) {
            this.printHTML = new PrintHTML();
            this.printHTML.setPrintParameters(this);
        }
        return printHTML;
    }
    private void logPrint() {
        LogIt.log("===== SENDING DATA TO THE PRINTER =====");
    }
    private void logAndPrint(PrintRaw pr, byte[] data) throws IOException, InterruptedException, PrintException, UnsupportedEncodingException {
        logCommands(data);
        pr.print(data);
    }
    private void logAndPrint(PrintRaw pr) throws IOException, PrintException, InterruptedException, UnsupportedEncodingException {
        logCommands(pr);
        if (reprint) {
            pr.print();
        } else {
            pr.print();
            pr.clear();
        }
    }
    private void logAndPrint(PrintPostScript printPS) throws PrinterException {
        logCommands("    <<" + file + ">>");
        // Fix GitHub Bug #24
        if (paperSize != null) {
            printPS.setPaperSize(paperSize);
        }
        // Fix GitHub Bug #30, #31
        if (copies > 0) {
            printPS.setCopies(copies);
        } else {
            printPS.setCopies(1);
        }
        printPS.print();
        psPrint = false;
    }
    private void logAndPrint(PrintHTML printHTML) throws PrinterException {
        if (file != null) {
            logCommands("    <<" + file + ">>");
        }
        logCommands(printHTML);
        printHTML.print();
        htmlPrint = false;
    }
    private void logCommands(PrintHTML ph) {
        logCommands(ph.get());
    }
    private void logCommands(PrintRaw pr) {
        logCommands(pr.getOutput());
    }
    private void logCommands(byte[] commands) {
        try {
            logCommands(new String(commands, charset.name()));
        } catch (UnsupportedEncodingException ex) {
            LogIt.log(Level.WARNING, "Cannot decode raw bytes for debug output. "
                    + "This could be due to incompatible charset for this JVM "
                    + "or mixed charsets within one byte stream.  Ignore this message"
                    + " if printing seems fine.");
        }
    }
    private void logCommands(String commands) {
        LogIt.log("\r\n\r\n" + commands + "\r\n\r\n");
    }
    private void setDoneFindingPrinters(boolean doneFindingPrinters) {
        this.doneFindingPrinters = doneFindingPrinters;
//        this.notifyBrowser("qzDoneFinding");
    }
    // Generally called internally only after a printer is found.
    private void setPrintService(PrintService ps) {
        if (ps == null) {
            LogIt.log(Level.WARNING, "Setting null PrintService");
            this.ps = ps;
            return;
        }
        this.ps = ps;
        if (printHTML != null) {
            printHTML.setPrintService(ps);
        }
        if (printPS != null) {
            printPS.setPrintService(ps);
        }
        if (printRaw != null) {
            printRaw.setPrintService(ps);
        }
    }
    private void logFindPrinter() {
        LogIt.log("===== SEARCHING FOR PRINTER =====");
    }
    private void setDonePrinting(boolean donePrinting) {
        this.donePrinting = donePrinting;
        this.copies = -1;
//        this.notifyBrowser("qzDonePrinting");
    }
    private void set(Throwable t) {
        this.t = t;
        LogIt.log(t);
    }
    public void findPrinter() {
        findPrinter(null);
    }
    public void findPrinter(String printer) {
//        if (printer.equals("default")) {
//            printer = null;
//        }
        this.startFindingPrinters = true;
        this.doneFindingPrinters = false;
        this.printer = printer;
    }
    public String getPrinters() {
        return PrintServiceMatcher.getPrinterListing();
    }
    public void setPaperSize(String width, String height) {
        this.paperSize = PaperFormat.parseSize(width, height);
        LogIt.log(Level.INFO, "Set paper size to " + paperSize.getWidth()
                + paperSize.getUnitDescription() + "x"
                + paperSize.getHeight() + paperSize.getUnitDescription());
    }
    public void setPaperSize(float width, float height) {
        this.paperSize = new PaperFormat(width, height);
        LogIt.log(Level.INFO, "Set paper size to " + paperSize.getWidth()
                + paperSize.getUnitDescription() + "x"
                + paperSize.getHeight() + paperSize.getUnitDescription());
    }
    public void setPaperSize(float width, float height, String units) {
        this.paperSize = PaperFormat.parseSize("" + width, "" + height, units);
        LogIt.log(Level.INFO, "Set paper size to " + paperSize.getWidth()
                + paperSize.getUnitDescription() + "x"
                + paperSize.getHeight() + paperSize.getUnitDescription());
    }
    public void print() {
        startPrinting = true;
        donePrinting = false;
        reprint = false;
    }
    public void printHTML() {
        htmlPrint = true;
        print();
    }
    public void setAutoSize(boolean autoSize) {
        if (this.paperSize == null) {
            LogIt.log(Level.WARNING, "A paper size must be specified before setting auto-size!");
        } else {
            this.paperSize.setAutoSize(autoSize);
        }
    }
    public void printHTML(String printer, String html) throws Exception {
//        setPaperSize("200mm","900mm");
//        setAutoSize(true);
        findPrinter(printer);
        if (startFindingPrinters) {
            logFindPrinter();
            startFindingPrinters = false;
            if (printer == null) {
                this.setPrintService(PrintServiceLookup.lookupDefaultPrintService());
            } else {
                this.setPrintService(PrintServiceMatcher.findPrinter(printer));
            }
            setDoneFindingPrinters(true);
        }
        appendHTML(html);
        printHTML();
        if (startPrinting) {
            logPrint();
            try {
                startPrinting = false;
                logAndPrint(getPrintHTML());
            } catch (PrinterException e) {
                set(e);
            } finally {
                setDonePrinting(true);
                if (this.printRaw != null) {
                    getPrintRaw().clear();
                }
            }
        }
    }
    public void print(String printer, String msg) throws Exception {
        this.copies = 0;
        findPrinter(printer);
        if (startFindingPrinters) {
            logFindPrinter();
            startFindingPrinters = false;
            if (printer == null) {
                this.setPrintService(PrintServiceLookup.lookupDefaultPrintService());
            } else {
                this.setPrintService(PrintServiceMatcher.findPrinter(printer));
            }
            setDoneFindingPrinters(true);
        }
        if (!msg.isEmpty()) {
            append(msg);
//            getPrintRaw().append(msg);
        }
        print();
        if (startPrinting) {
            logPrint();
            startPrinting = false;
            if (isRawAutoSpooling()) {
                LinkedList<ByteArrayBuilder> pages = ByteUtilities.splitByteArray(
                        getPrintRaw().getByteArray(),
                        endOfDocument.getBytes(charset.name()),
                        documentsPerSpool);
                //FIXME:  Remove this debug line
                LogIt.log(Level.INFO, "Automatically spooling to "
                        + pages.size() + " separate print job(s)");
                for (ByteArrayBuilder b : pages) {
                    logAndPrint(getPrintRaw(), b.getByteArray());
                }
                if (!reprint) {
                    getPrintRaw().clear();
                }
            } else {
                logAndPrint(getPrintRaw());
            }
            setDonePrinting(true);
            if (this.printRaw != null) {
                getPrintRaw().clear();
            }
        }
    }
    public void print64(String printer, String msg) throws Exception {
        this.copies = 0;
        findPrinter(printer);
        if (startFindingPrinters) {
            logFindPrinter();
            startFindingPrinters = false;
            if (printer == null) {
                this.setPrintService(PrintServiceLookup.lookupDefaultPrintService());
            } else {
                this.setPrintService(PrintServiceMatcher.findPrinter(printer));
            }
            setDoneFindingPrinters(true);
        }
        if (!msg.isEmpty()) {
            append64(msg);
//            getPrintRaw().append(msg);
        }
        print();
        if (startPrinting) {
            logPrint();
            startPrinting = false;
            if (isRawAutoSpooling()) {
                LinkedList<ByteArrayBuilder> pages = ByteUtilities.splitByteArray(
                        getPrintRaw().getByteArray(),
                        endOfDocument.getBytes(charset.name()),
                        documentsPerSpool);
                //FIXME:  Remove this debug line
                LogIt.log(Level.INFO, "Automatically spooling to "
                        + pages.size() + " separate print job(s)");
                for (ByteArrayBuilder b : pages) {
                    logAndPrint(getPrintRaw(), b.getByteArray());
                }
                if (!reprint) {
                    getPrintRaw().clear();
                }
            } else {
                logAndPrint(getPrintRaw());
            }
            setDonePrinting(true);
            if (this.printRaw != null) {
                getPrintRaw().clear();
            }
        }
    }
    public void appendHex(String s) {
        try {
            getPrintRaw().append(ByteUtilities.hexStringToByteArray(s));
        } catch (NumberFormatException e) {
            this.set(e);
        }
    }
    public void append(String s) {
        try {
            // Fix null character for ESC/P syntax
            /*if (s.contains("\\x00")) {
             LogIt.log("Replacing \\\\x00 with NUL character");
             s = s.replace("\\x00", NUL_CHAR);
             } else if (s.contains("\\0")) {
             LogIt.log("Replacing \\\\0 with NUL character");
             s = s.replace("\\0", NUL_CHAR);
             } */
            // JavaScript hates the NUL, perhaps we can allow the excaped version?
            /*if (s.contains("\\x00")) {
             String[] split = s.split("\\\\\\\\x00");
             for (String ss : split) {
             getPrintRaw().append(ss.getBytes(charset.name()));
             getPrintRaw().append(new byte[]{'\0'});
             }
             } else {
             getPrintRaw().append(s.getBytes(charset.name()));
             }*/
            getPrintRaw().append(s.getBytes(charset.name()));
        } catch (UnsupportedEncodingException ex) {
            this.set(ex);
        }
    }
    public void printHex(String printer, String msg) throws Exception {
        findPrinter(printer);
        if (startFindingPrinters) {
            logFindPrinter();
            startFindingPrinters = false;
            if (printer == null) {
                this.setPrintService(PrintServiceLookup.lookupDefaultPrintService());
            } else {
                this.setPrintService(PrintServiceMatcher.findPrinter(printer));
            }
            setDoneFindingPrinters(true);
        }
        if (!msg.isEmpty()) {
            appendHex(msg);
        }
        print();
        if (startPrinting) {
            logPrint();
            startPrinting = false;
            if (isRawAutoSpooling()) {
                LinkedList<ByteArrayBuilder> pages = ByteUtilities.splitByteArray(
                        getPrintRaw().getByteArray(),
                        endOfDocument.getBytes(charset.name()),
                        documentsPerSpool);
                //FIXME:  Remove this debug line
                LogIt.log(Level.INFO, "Automatically spooling to "
                        + pages.size() + " separate print job(s)");
                for (ByteArrayBuilder b : pages) {
                    logAndPrint(getPrintRaw(), b.getByteArray());
                }
                if (!reprint) {
                    getPrintRaw().clear();
                }
            } else {
                logAndPrint(getPrintRaw());
            }
            setDonePrinting(true);
            if (this.printRaw != null) {
                getPrintRaw().clear();
            }
        }
    }
    public void OpenCashDrawer(String printer) throws Exception {
        findPrinter(printer);
        if (startFindingPrinters) {
            logFindPrinter();
            startFindingPrinters = false;
            if (printer == null) {
                this.setPrintService(PrintServiceLookup.lookupDefaultPrintService());
            } else {
                this.setPrintService(PrintServiceMatcher.findPrinter(printer));
            }
            setDoneFindingPrinters(true);
        }
        getPrintRaw().append(hex2Byte("2770302525"));
        print();
        if (startPrinting) {
            logPrint();
            startPrinting = false;
            if (isRawAutoSpooling()) {
                LinkedList<ByteArrayBuilder> pages = ByteUtilities.splitByteArray(
                        getPrintRaw().getByteArray(),
                        endOfDocument.getBytes(charset.name()),
                        documentsPerSpool);
                //FIXME:  Remove this debug line
                LogIt.log(Level.INFO, "Automatically spooling to "
                        + pages.size() + " separate print job(s)");
                for (ByteArrayBuilder b : pages) {
                    logAndPrint(getPrintRaw(), b.getByteArray());
                }
                if (!reprint) {
                    getPrintRaw().clear();
                }
            } else {
                logAndPrint(getPrintRaw());
            }
            setDonePrinting(true);
            if (this.printRaw != null) {
                getPrintRaw().clear();
            }
        }
    }
    public void printPS() {
        psPrint = true;
        print();
    }
    public void linedisplay(String port, String msg) throws Exception {
        SerialPort serialPort = new SerialPort(port);
        serialPort.openPort();
        serialPort.setParams(9600, 8, 1, 0);
        serialPort.writeBytes(cls());
//            serialPort.writeBytes("0123456789012345678901234567890123456789".getBytes());
        serialPort.writeBytes(msg.getBytes());
        serialPort.writeBytes(hex2Byte("1F4300"));
        serialPort.closePort();
    }
    public void append64(String base64) {
        try {
            getPrintRaw().append(Base64.decode(base64));
        } catch (IOException e) {
            set(e);
        }
    }
    public void printCard(String printer, String cust_no, String cust_name, String since, String valid) throws Exception {
        appendEPCL("+RIB");
        appendEPCL("+C 4");
        appendEPCL("F");
        appendEPCL(cust_no);
        appendEPCL(cust_name);
        appendEPCL(since);
        appendEPCL(valid);
        appendEPCL("I");
        print(printer, "");
    }
    static String readFile(String path, Charset encoding)
            throws IOException
    {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, encoding);
    }
    public void appendEPCL(String data) throws Exception {
        if (data.isEmpty()) {
            return;
        }
        getPrintRaw().append(hex2Byte("1B"));
        getPrintRaw().append(data);
        getPrintRaw().append(hex2Byte("0D"));
    }
    public static void main(String[] args) {
//        args[0] = "method";
//        args[1] = "printer atau port";
//        args[2] = "message";
        Logger logger = Logger.getLogger("qzlog");
        FileHandler fh;
        String body = "";
        try {
            fh = new FileHandler("qz.log", true);
            logger.addHandler(fh);
            SimpleFormatter formatter = new SimpleFormatter();
            fh.setFormatter(formatter);
            if (args[2] != null && !args[0].equals("printRaw") && !args[0].equals("printHex")
                    && !args[0].equals("printFileHTML") && !args[0].equals("printFileRaw")) {
                java.util.Base64.Decoder decoder = java.util.Base64.getDecoder();
                body = new String(decoder.decode(args[2]));
            } else if (args[2] != null) {
                body = args[2];
            }
            if (args[0].equals("ld")) {
                new Main().linedisplay(args[1], body);
            } else if (args[0].equals("printHTML")) {
                new Main().printHTML(args[1].equals("default") ? null : args[1], body);
            }else if (args[0].equals("printFileHTML")) {
                Main m = new Main();
                String s = m.readFile(args[2],m.charset);
                java.util.Base64.Decoder decoder = java.util.Base64.getDecoder();
                body = new String(decoder.decode(s));
                m.printHTML(args[1].equals("default") ? null : args[1], body);
            } else if (args[0].equals("print")) {
                new Main().print(args[1].equals("default") ? null : args[1], body);
            } else if (args[0].equals("printCard")) {
                JSONParser parser = new JSONParser();
                JSONObject jsonObject = (JSONObject) parser.parse(body);
                String cust_no = (String) jsonObject.get("cust_no");
                String cust_name = (String) jsonObject.get("cust_name");
                String since = (String) jsonObject.get("since");
                String valid = (String) jsonObject.get("valid");
                new Main().printCard(args[1].equals("default") ? null : args[1], cust_no, cust_name, since, valid);
            } else if (args[0].equals("opencashdrawer")) {
                new Main().OpenCashDrawer(args[1].equals("default") ? null : args[1]);
            } else if (args[0].equals("printRaw")) {
                new Main().print(args[1].equals("default") ? null : args[1], body);
            } else if (args[0].equals("printFileRaw")) {
                Main m = new Main();
                String s = m.readFile(args[2],m.charset);
                m.print64(args[1].equals("default") ? null : args[1], s);
            }else if (args[0].equals("printHex")) {
                new Main().printHex(args[1].equals("default") ? null : args[1], body);
            }
//            logger.info(args[0] + " " + args[1] + " " + body);
        } catch (Exception e) {
            e.printStackTrace();
            logger.info(e.getMessage());
        }
        System.exit(0);
    }
}
